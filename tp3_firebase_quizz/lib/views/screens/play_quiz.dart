import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

import '../../business_logic /blocs/questions_quiz_bloc.dart';
class PlayQuiz extends StatefulWidget {
  @override
  _PlayQuizState createState() => _PlayQuizState();
}

class _PlayQuizState extends State<PlayQuiz>
    with SingleTickerProviderStateMixin {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  // Liste d'URLs d'images correspondant à chaque question
  List<String> imageUrls = [
    "https://images.radio-canada.ca/q_auto,w_700/v1/ici-info/16x9/abeille-seule.jpg",
    "https://www.thetrainline.com/cms/media/1360/france-eiffel-tower-paris.jpg?mode=crop&width=1080&height=1080&quality=70",
    "https://img.freepik.com/photos-gratuite/peinture-lac-montagne-montagne-arriere-plan_188544-9126.jpg?size=626&ext=jpg&ga=GA1.1.1826414947.1704326400&semt=ais",
    "https://www.francepodcasts.com/wp-content/uploads/2019/06/Question-douverture-a%CC%80-la-PO-du-DALF.jpg",
    "https://lejournal.cnrs.fr/sites/default/files/styles/visuel_principal/public/assets/images/700-07760424_72dpi.jpg",
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFhUVGBUXFRcXGBUXFRcVFRUWFxcVGBcYHSggGB0lHRYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAEBQMGAAECB//EAEYQAAICAQMCBQIDBQMJBQkAAAECAxEABBIhBTEGEyJBUTJhcYGhFCNCkbEHYoIVJDNScpLB0fAWNEOy8SVFU2Nkg6LC0v/EABoBAAMBAQEBAAAAAAAAAAAAAAIDBAEABQb/xAAzEQABAwIEAwcDBAMBAQAAAAABAAIRAyEEEjFBUWFxEyKBkaGx8AXR4RQywfEVI0LSUv/aAAwDAQACEQMRAD8AXpEM68gexwz9nI7ZEYs+wzL40041C1p4MLWOsyIED5GTKt4pzlRTaAFsOM0WzZUZEwwICaXEKbzs2XwUZotmZF3aFTuLyPyc2HOTxjBkhELlRDnjCINGXIF8DJY4wc7hbbiXuMWT2MvdGQaBF9Kj8zjHS6cDFi6r7Z2OpVkT21HK9jmNT+IAZOpyup1XOv8AKjXx2yc4d5TxXYrHvxPo9b/n88d8eVEQPewTf/nGRHXWMqnTupf+02s/V5if7qqR/wCXEuoG3X57ohXGy9JEmYZMVJrQbF9jR+3AP9CM4k19HNFEnREaoCbGbODNi1NXeCarXVfOE2iSYQuqgCUd1LUUOMrGs1G40B75JquokijiWTV0fnPUw2HLRdeXisUOKtejlArGD6oAZS9P1E3zjL9r3DF1MKZumUsW0iycnWA4FNJzYxa87DITqecNmHjRY/EgppLqKGCHqGBvIT74GyG8oZQbupn4k7J2/UeMVzahq+chF5zJJWMbSDUp9dzhcqBtUwOak1zAd807A9xkZhx+UbhS9o/YqFtUTmZvy8zDhqXmfxT2GQZIVB74Csn2yZZMURuE9r7Keq7ZIso98gTOymCYRidlLtB7HI2hOcAEZ0s+ZfZbY6rkJ7HN/sd9jkgdTmFiO3OZmK0NBUf7KQM2qH5yQTD3ycAH4wHOO6NrRsoEse+Sq+b8vOghxZITACF2Dmil5iZIoxZsnBQlKztCcmC3mxFglw3Whp2UWo1Plo0hP0gn2Fn2HPyaGUmHVhXWhciv5gf0jd9K7fqvaStbiK9Xzlm8TaFniFNShhvUcbgTQ5+xN1/yxTp+j7p9zcQhCCSONoi8uga7/b/jnm4xwL4zFsCevrfp8HpYKgXNLjF7eIi3I8DrwRvhzqrSzSEqwWYkoeDQQUAwBtePc+4A9xjrUjEvg/UR+qNX3UX2D1GkJX+KqPIJ/PH8i5ThKmdmY8TyU2LpCm7IDNhN999NuCh/aSBWBagsecKeOs5C5a2BcKF5cRBSuW8h8q8anTXnJ01Y8VAFMaJKWpFhkPGd+Vm7zS6VzWZVhGcND75IGzYbBmEdiolXNMoOdM2cBs0SsMCyglX4yCRcMo5yYvtjA6EtzJQFfbOlW/bC2SsiJOEXShyQVryxmZnmZmZdHZFbARyPzGYsdduc9Q0vSYxEImUEAe/OcP4dgNfu1ofAq/x+c8n/ACbZ0K9U/TDa4leaqubIOXnU+EIuSrMPtd5UZNOykhlPHyP1yili2VP2+qnqYR1PVBkn3zgoMM2A5E0Bx4qKZ1IoQpm0c5KU+RmbMLOEvszMroOPjOg3xkROdBsElGOCLhnPuMKQg4s352k33xTmynMfFimQjGdiPBIdR84YkmIMhUNIKkSLIYunETtJvPKgbOKHP1fPt/XDIjhET+oj+6Pf7n+H27jnJaxEsn/6HnBVVEfujgfcJX4ihk/Z38tAxFEgkj0qdxI+Tx24yp6iSdoDuEdNQNK1kEj3LHLXrurD9pk03/0kkn+LdVfjXP55XdXpmbTFVNEAEHt9Juv0zzfqJhzSfl16/wBLEseJNjsY238k96N01YkYou3cRfAHZR7Afc84RIDhXSumeTHtJ3E8sf6UBwOKyd4hnp4cllNrXaheTiIqVHPbYEpSY7yIp9sbPBXtkSaUsaUWcpFUKU0roDJIdO7g7VJrvWWXQdDjHMnJ+PYY3HTo9tKAAe9e+TvxjRYKhmDcbuK84lIHfvg0kmXDqvhoGypo/Jvn8fvle1PRZF7jt7j4yqliKT91HWw9VuyTvIc0r4Y+jJwd9I3xlgc0qI03hbvOlOaTTGsJ0mgZzXYfOAXtG6YxjjFkOiljSgk/GTHTSXt2kZbOjdLSM33PycZ+Sl3QyGpjgDAEr0KeBJEuMFUSLo8jEWDR+MIl8NSEUoF/OXyDTirrO3UYg/UH7QqB9PpxdeWzdEmVipA4+O39M1npDbftmYz/ACb+CX/jKfEpsvyTnasPnFW8kYZpW4zyCvUIRZwOXQKxawCGFEYUxzFOdKyLKt9Z8NhgDGdpHtXBGVXUaSRGKlSaNWAaPvx+WemPJgs2mD8e2VUsW5gg3CnqYVrzOhVO6Z0YygEkgXzxjSTwohHBYH8ssUUSxrXxnCaontmOxdQmQYWtwtMCCJVEk6Tsk2ScCjR9j+GC6vpDLyKIJ4r9M9A1+nVxyARirp2mbexYcDt98oZjXRKnfg2aKmy9PkQ7WUg/hh+n8OzN/DX496+ay+cfAwhVAwXfUHkWAWt+n0xqSqzpPCaAetif0wjV+HYwPQSv65YMiaMn/lk36mqTJKp/T0gIAVNeAxttJv753p5CZCvPduKFcCP8/nD9K6yaueEhd0KxGh/80O3J/wAP65VuldWY6yKP9mlEgmkDimAAYkMbqiqqd19jtFe2KxuIfNHI2e+CekEfyFuHoDvybQQPP8eqWdW0UcWuaadnRtzgOWStjac0m0G2W2UdrAPyLyROp6Ty9vnWSP8AUk9/8OQeM5xLrZD5d+WzJ9Qoigm4D59Pbgg9rzk62UAER9lIoB6oBj2rnlfxO5P9YVPVl5vxPqr6Dgwd0xYTbcK9aXVxzLviYMBwa9jQNEex5GdwpzzziHwR1Qh00vkoA+5iyEBlIUm5LHrsKoB7/bg1apowDYy9lcubdea+iGutoo2A7EcZzEQnbvmO/OaLjMzFblC3LqmOSabUOP4jkIN5KOMHMthGXxzizqxLekDj3++Fq+Ryy85zHwZXObmEJf0/pYBtufgY0m0aEfSMgWfJRqMJ9VzjJKxtNrRASiboVkm6+3fD4tDsXJ2myKafDdXe4QSsbRY24C2o+M7gejgqyZhmxZKNNG1fsMHk1JqsCafIJJ8wBbmU/nZmL/OzMZlQZlZYsK0/GAI2Tq+SqhHSzUMGk1ddsGm1GCGbOXJg+s3e1ZJHqqxcHzoHOXIrU6q83p3FYATzmjLmoZTFpb/AZwr8/bAvPyRZc7RcmSvZzsS4vSbJVk4wVqPTUAZptR8++KqJN5PPAJUMbEgMCpIJU0RRphyD9xmlYDuvK+h+MY4+qaqeV9sM9qG9TVsIERpQeCoP88ih1cwljeKeOYCT0Mm/1hG/iG+xfY2fc5qfwrF5e0RncH2dyWPxXz+mb6Do0gVgOzeYyvYpAqq3c+3c39s7EYmm8dwEEeVh/XknYfB1WXeRBvvNzKE1kmsUNIoREZ2IZIh6iW9aofhS1cdv50eP8oWn796YAiq288jmr7f+vtlm6r1OGWJIozQjvao7FQvAr8v0zcEI2Rj7Kf1yI1cxtfS/urKVEFpcRGtumiQeHOp6lNVB+0FSruyBioDWRt233P1L3+c9A1msQTJCb3yLI6/FRlA1/f1j+RzzPxOsp1KBeAqB0JcL6iW3bQffhcY9a6069V0fmAKyxosgBBAeferC/ccqfyGejh2BzQQbkEkcLEjzXl4oup1XAiwIAPG8FXgx5yUzDNkZ1GYhspTxnBlzjzc1YOcuW2nwd5DhAjzGXjCELChhIc6E2GNEAt1ZxLKTeGACsJhHtqhg0mqwSic6EB+c7KF0qQ6nNeec1+yn2yaHSEnOsuuow5OZWMItCcyTSn4zg5blKX5rDv2E/GZhZ1mRM43AGZLqMradVzZ6lk2Up2cJjLLnKyYu/wApDOT1AYQaUGYJyk2EiXjK4Ndkya/OLSuzhO7OQTNgI15zjzS2dlXZgp2nrORrDkZTOGiObAQyi01mGRazE1HJEvOLV2aE+inyd9SEUtxwDQJoE1wLxBLrFjRpHNKilmPPCqLJoZWPFGuQatPMLlTGpUAMVY/veB/D2o97sAe4wHgtEwm04e6CYRfR9XqTPpt5Qq0yFk8p0YW3JBLngX7jsPbIdXKVE5WPiNZSHLbRRQ2VG07gBgnTOoJM8vlQsuza1vtABZQAavhrX7dsX+K9Q0cWw+lpD/Dt5Uj1XRJr/nkdOq2pXFH/AKJFvC+nASrqhFGm5xdIA3nY80L4c8SaiaTyppSylG2ghfqCxgdh/qx/1+Tl/wBH/o4v9lf655T4fkYM5RFZgordXp/eICwP8PBIv756voPpQDkBUFg2Cbo1/wBe+Px9OlSrBlNrWi1hA5kwOoHODCV9LqF1A5iSe99v4VS8WndqIxSnagPMioeWa+CMWdY6VqdTqZtQzwAuVYbZbAQquwBq4pa9hzeWrqeivUKwJBMQB7VxJXuP75xz1mCppFXikhA/BVXj+QwaWIqUmAsMWHD+ZTauEp16xDxYlx32PKE10Glfy1E23zQi79hJQt2JUkA1YPt75zLo/jGLL++Yc/Qp9q+tvzvCFQXjW1JnkVAWadFX307DMRTj7UAfGAOoGMD0JbCCs51fznM01dsjSa8KEM3TCWUBe14nnFnC2lvgZEyE4TRCx5lCqmTKc4bjO4lvGOA1QNN4U0eEq4wHyW+c5MhXviiJTQYTmOYZN5oxF5+b84n3wMqMOTvzRm8Q+YfnMzsvNbmVXRWPYYRFp2JqjloigRewzHVfYY7tBwSOztcqt6jT7eCMjUYz6nHZsfnkGji9XbtlrGsyZioaj39plCO6f0beLJq8i1XTihIB7Y40c4AzWuUPyDzkBecxV+QZUjUVhMTZrUQ1kIkzdUGiaIcnjT2oYpTUVhCazFlpRhwTtNKtdhnY0S/GLI+o4XHrAcCHJktSrxl0TzdPsVyoLoHAA9Sk/Tz25o8fGV+byTpoYFcy6iAoCWHqQM4VFJAsgekd+dvc5buulmgYo4VlphYBDbf4efn2+9Z53MVg1Uzc7ZH05quFbzFZh8m91j7g/GLqPf8Atm3D5ysq8Oyke8ReddtNPcpD1Ppmo1ADhQVoE/vA2569Tfb4A9gB98ZeDvCqzQv5kahkk23uf1WLv0sACO38sba7RztplETMiM77VH7uyJlkUjs30RtxXvlh8HRSeTKXBJ80n2HBsgUOCQCo5+MM4up2YY0gAbCQfe6wYSmahe8E9YI9BbkqYmhSCWZtNIm6NCj7CWIJkQUQ9g9m5F8qR7ZavDcrtuLNuIkAHCilBFD0gfJxHpujOjzB6tyWJINKoYBQaP8AeJP2CntjmKYqzujR05PK8KGAsE/f6u32/NT+yc3vXqGXZiASAMndza3GZwv7hT03PZXzC1MDKQDuc0mNLEgE9ToChuux6gzgREKjRBbIVgG8xiRRqyRt7EdsZdPmeZRJIpLlFQssrKrNGNpYAdrrtlG1Xi9mlZoirovpWwfWwN2t+1mr96/lF0zrbowPlxIoYBmUNtq2tSwP1D1Gv7pOd+mrlgAAjw/tU/raHamZmfnLqfg9c02tVtQxtbqNfk2Yi5Xd82RjcOBnnHhfVTyuxXy1UzBix3bwFRVC1dWFWucf+MetGHSTSISGC7UPuGdgoI/NsVg21XGoHDV1r7EC3pPigxOVhHIXnknuqlwOckjjIIeorIiuOzKrD8GAP/HNmYZYAVMXAoOQG6OTKpzJDnLSYySUEKRTtORSag5E0uDs+MazilufwXUkhzI9RtyBmOcbSfYnG5ZEJYdBTKLXi8lfVIeSPywZelvQPzmlBXg8HEFomyeHHdFK6nsB+eDahT7YVHOPfMfUjB0OiO3FKrb4OZjH9oXNYXgugcUGuqb4OF6eYn2zvafjJENZxjghuoJUYmgMk/yfQq6+cm80/OdeZeH2hiAg7MTJQh0BA4bnNrppO95NrNSkaF3YIoq2JoCyAL/MjBdB1rTytsinR2omlNmh3OLzbI4XLhvcZC8fuaH4muceKt5X/GHQH1kaRxuilX3HcCQRtZa49+c4kgWCwtU0Wm3diOO/N1k6dP8AvnnPS+rN0qR42VJDKkLn1bCvDELVG/q756r0efz4Iptu3zUR6u63qGq/fvgtqZrLICHTp33zvUhIU3yMAPazVn2UX3JwvWayKAXKwXiwO7ED4UcnPO/EGvOqlIjLFWf92CWWlVFHCsPTdbj2NnMfUy23TGUwTeym6v1k6qmEe1YmbaLs+1sfvxwO2I/FWrRo5ippvNjaro0eVr5IKt27WMZy6F0VUa1rltp5ugK4HNMbv9M46j0dI4o9RvJeUimYKYxYPrIvcw4v5/PEtLSSXa7dVQMwEMFtT0+f2kUHVZdkStX7ti3YAnngk9+VPbHej1LCVladoxIu5Ujl2q7Ar6wVK8EEiiA3p7cZmh8Lu5TfKoeUFkAFFwBZYK0lng5P0npbxdR08J2PtkVz6CHQUSTusivzx9Z2HNNraZJcNbEA8eG+k7WSaTcS1znVWgNdEXEt4c7jVT6piNPqXEm5naJPS6syq7ncaVj3urPxxi/wnCVi1EdkKnlEFiFAL7uOT247fcnPROpdG82F128twfYkd+DRohlUg0e2KOneHDpoygLqGILOzUS10NxrsATQ+5+Rk36gk5zGaQdBFojlAjgqjhabW9mLtuNTN9d95mZ2VB610yJHBRwC1cCm2kkbnAW6Ukkm6+3wBerKhcwqpVIrRQdoZmjd/wB4a4uya+BQz0Lxj0ZtqzIyFvLSO+bBUswJ5HFn7ZXo/CLqnmSyBFNeptoW2NAEtJ3JIH3y3C42lOaqdJgAHf7bfhefifp73CKAmdSeR08TcxGnMpj/AGbahLZGcmW2JUggbAleYOebJF8Dn8ck/tV1wEMUI7ySbj/sRj/+mT+WT+ENA0WsMTlS2xiSF2nadtAjcb7d/wDnlR8UaqTWax2SOQpHcca7GBCqTuJB7EtZ+ewPbMwnZ9qXTDQZvb3hMxvaim0EEuyxYX1OsTtv6aq6eBtQsmjS+WjJjb8uV/8AxK/yx2QuUPwW82mn2SQyCOalPpJ2sCdjcfiQfsb9s9FeJcOvk7Q5HAjkZ+XSKGbsxmBB5gj35ITeM4dhhJRfjIWUYoJpCHIGa8m8lK/bI4pBQI9wMMkoQAp9P08XZN4xXTAiu2AR6uvfOzq8U4uJTW5QExdwBQOCyRBu/OBmQntkiM2YBCKZQ2u0RH04qkVh3x7I598FmQH4ymnUixU9SnNwlO45mGeWuZj87eCTkfxRwkb4yObUbfqZR+JA/rnm39nWuZdUyyM4RoiVMm4L9StdtxyAxB+FOBf2gxu2pllA3RXsDrW0FFG5TzyeQb+/zwIi7uyAqLzEr06LqsbOI1ljLnsodSxrngA4cpbPDvC0gTVRTOwjjjYF25JNAkLtFk81+H5ZavEXjhmk/wA11NJtXgoVbfzYG5b7VgdqAJI8lt9ldvFHTZdRpnhj27m2kbiQp2sGokA12zziFNT09lmKJubzUH1sAI3CPfArkCvt8ZbPAPW5W806qR2t1VC1EcA7yKHayov7ZaZZgNLKzEBd2o79v9NIAP58ZkMqSQbjXkugpX0R9W+mWaSePc7xtsUhPLSj6S3uOVYj7Vz71DS9Gi0Wrjkl1aMIWDlURi5Kn6D6qUn7n2zt/FUo1rpH/wB3V2kXcCE2lSQSgG4glu12b7AWAV0rRRzg+VGJZ2fcN12PUbZxdBbK/wA8nc8tMOIJ25JxFN37ZspNT4hEpQQ6RC6VtkkUOWXaV7Aeyvfc8jONbr9eb3vJxRJWQoAvJrbGQtVxZGF6nVtAHiliMbKAvoB2q3B9LfB79/fKl1tUnEY8xo2WwSULKxY+9H2xbqhc6MyIhkJtCz7lZ9pJJshh2o0SW5/X3wrpMqCSI9x6q+5289/wBxX4T1EWnBBl3WwN0wUVXYWf55rpeuPnnYpKqWYUPlVHf25Hvhsc0WlcSLBW/qjgp5lgAgbb45JJ7n8Blf8A+0hdtNGyovlB62jeVYKU99ykmyb4qjzk/VtQ8ymJFraQ3PcgA9gtk80Mrc3SpF27q4Zb7/USOxrke9/bHAA6rpLdF6Gmq80xsWkZozuQ/uxtfaybhUo5Kswoiue2LfF3UWVY2Vjv82IC1G5rNOu5d1WCTVgfj2zvo3TEKKShLccsWFnk2D9izH/Eclj6OPNWa2URAqoYWtkAEgdyaoCu1c8cAMrgbwjkHSUfPoZV3fvuFNn0iwN19gOOPT+FHAdRHJ5bOJd2zmmQD1KCeavvx/u/fC9YWIbYu5+4JosfwHt+HOR6rTurO12OOCjJxtG7m1LN7XXHx8iKXGfRONe9mj5v89FWOi+NX1BSNvRsAIOzduIDDd3bsD717Zbj1DzANzkgEECoqDAEWFL+9ng/P2yjeHukCWN2slklLABmBNKOR8nmq4u/tWPdHpIhGZJV9KoWdtu6vr5A+QGf+fbtTX0+8csD51UtOocgzEmeiJ671XUefpF073KZiFDil2sPWCyqQV+Rdjiu2Fz6MzayZxpWVgQj06bWdCPX35tQB29/nK5q+mN+3RyqpMalfTyvpCluBXB9R/lXc4VPq5o55tjuIy7NES8htT6tu3eCtFq5AsLgQXDKnghhzg/yrTLt0SNq5YG2Ip5BUsAzGgBfemVefj2s4f0HqaauESofcgqeGU/DCzRxTptTMyhDIfUCP4u5Xju1E2CaPzkPQepQ6eZllhaJqWETtYRxFS+rilqu9kcHkXydFrv2AX18PnD8pVdwPfceXzbzgc1ZJCu8R2N5BYL7lVIBIHwCw/nnY0uebdT8SM/UV1MfMcZWNf70dkOa++5iPwX4z0jR9QWVN6G1tlvmrRip/EWDz798pqMLC0HUiVJTe1+YjYwl/XdQIIJZj/4aM34kDgfmaH54h8GTMYE07oweNFNmirKex3A97vg+wyT+0p2OnRfLLKZAz8gLtQfS3PN32/u/NYN4J6sJ5BaBHjiKKNxJZdwP2uvzzstSA4CRvpbSJvN+h8NyzMktJg7c+MW2VkbT5FIgBWzW41+dE1+mMWOU/wAdSzE6ZYYmkKS+cSosDywV2k/fc38jnEwLkAcSYCyOAnorIqVnZY4tTxBAY/MLheF3KSNyswPpIHNggg/hhWt1eyPzArMDVADnn5vtnWC2+y7ZjkZyRTYDcgEAi+Dz2/rmimG3kgKgrMyKbWQKxVpACO4+M1h5gsyrzSXRKb8qeNkVAxALb6UeoAEfSDYHb27ZD02f94qgt6zsb1DlX9LXS+44NY96F0BYCdx3712EAADkgsPkmv0OY/TNPHK0qkqfTsUeraysS1EnixQ+ftnkEimJcfuqHMkgmyk6n4UZ02RkGPaJVJsEsaO1iAbG0tVdrOLP+yQjtnkVTZCAsxLckN6Qo4o13P65ZtC2r2BYlcAADfJtuqqgW9h24+MjPQSxubUxrXBANk13tjWA7EyJaPE2HzyRET+1v2Q8Kktsi/etXNABRXF2ar8eO+GTwTFdmo1Sxp32Bt57k8gd+5Pc5NodfptOhCXuNbiBZJB4JJ7YtXqyiV3WNTuIon6uwHftkoyMET1AmPTXzhY4tI77hPLT0v6pXotBHJqZlBlIUJTAEVwpNhQfqAFcfOMeoQnRRmaHUyebGVbyz5YVhuAPG0Eij7/GR9C6oW1epkNKrsgb3ICptFH8h7YR4s6yjLJAtEbU3bRY3+coI3m+w4oE9+az0WHvtaJI7tttB99JSgxnZmoAJGaDpx+BMptIJ0R5S8zSckIW8vcpqQJVCg3pvJB4dhMf+iEZFkktuPHt9Xf8sL6bqIzv8pQu2QBqUgEiMDjiia23WSeedik2bo/lxZ/liMrTeFZkGp146pcvSYE4EaN2H02L9P68nI514CqAByAoAAvax7fkMNeUWR7g7iBZ4DD3HHYDB327uByh3fjY9h+BxzW5TICEwhtJIPUxUluw5Cjduv1Eg0PwB5H3zfVdSihjtarWtzLyCPVfp4N9q49+M4ihYyAi2BN7LoXyeSPwP65JremSupUKELGuXv8AUXQ/LGC+qGYRvRInDM+642ijCLf0uN+87a4sFO3wcmaUecsNHe4OxfnaFDNd+mt4POTdKiOxeRaiiTRuuCAaGKJtQYtRNqxL+7TTsSfq7q4BX77gvb4++G42lY3VMtb1HTpFqQmpInTgMm0epBZVQe4u1JHwfjKd0TxVLC2yZzPETZVzcy+5ZGY23+yT+BHubP4QiLQGaYRtMkpdkIBhpUKKTZJYXR9vYD3NTg6JqWnmijIdINm7zmIvcu5Sljct1YPtxeIblcM0/Jix680YMGIMfibq19B1SLDK1mpZ5FjPNEmPzB9wdqk85ZdLCkkWx1JVlpgW77hzW02PyyrL0yeLQvHNG0LedHIhbjduUo4DCuQB2+/asax9VaP0CMlg6IKtg29Im3mu3+kI4v6TlIcCTHFIZ+0E/D8CeMgBPpPFjhuPp49jzYAPPY39sXdS2vIihSCLPY3Rrn29936HD+oMUWQqAxCk1ZAsdvYj2OATx/5wOL9Kkc8E2b5OCBBmU3lCI6nCi6eQtyoUsdoJ+mi3A+w/TFvS9PuDGJGXkqbVV3cFbtgexANijYH3BfajqEcZVZCqFhYHJ4FfUAOP+qyndT8ULHK4XTvNHA22ablQrXyAwHKg+9A2bBPFg90mAU1tNzWh5BAOhgiY1jYxvw3TPSTJpqhKQoeNpcgs1rZsgivc3Xthmg60mmVCZFaFyAa52tt4KAEkXtJrse/3Mei0cUqpq38qNGiWRy0gCrtFE8ncQL23XahkcnRIpyssE25GT0cMtofSWCOQ2323AV25zaQa0zpPBDXe+oIN40n5PhISjrfixNTKunkh2wmRaKufMJ52kjhSDY49vk0MP6ZpESZ28gJGgRoZCdjFrIdGJkqzxX08MfwMOs8I7R5ok+gFgVI+ASQRdj6eRxyPkYb1OFpOnzKxuoXY/V3RS3e/tj6zszv9RIHU+cWHUaJFEZG/7WgnXQHwmJjhPkneq6/GqmvU/qHpDsm4HaCGC8qW4+9Yp8N9Mljk/wA5fesiMaPCqTIzv/MsTgukRhEtIGFAD1tdVX+ofgfzznqzJqJOnQtKioTL5iBwbKhPLUjiySGWu3f3rJqwfVpupEwCL24CfOydSFNp7SLjS/G3lfeYCr+r1tn9lEKq7OZJmax33MoVhyRtcc1zxlh13WZmjYkRyKACycBQFN7/AFKTxz7/ADgnifpyuYS/7uaRnhVByfLRpNjV7g7QRfs1e2Bajo0+mgd3kQbQwQeoFmcEAGxx3+/bBNZoAAA5g3kze6czDPqBzvW1tIsevuuvE/WW1TJFAVEKkDbuVWI42km/R8V9/wAKJl6hrG1EckUL/u4TGRaN6m3AtwTd0huu6Z5z0yQgjg1Yvgdgebuv+jnqfhXqmgdZWlklSUUFC2jFDXqXaSCdx9z7DjGuFSCQIAAvfbQ3lQsyuME3OwXnOr6skbskiSeYpp7Z/r/i7n5v/me+ZnoBPTpCXnM7SEnc1Ibo0p5v+EDMxPbs+f0qf0jvkfdDyo7ksIfLB7BFrj7kDnJtrwxiRQ20+l5NgOwtxtv6hx9gPvgvh7xjveGOcBQQAz+raOKplWiB+B/lhXUvD2t81nh0xKlmpVaNQVokEl25qh7/AMRyaphOzd3pJPh6/eQmYLCUq2Z9SplAGu8zs2xdadL8ASYRPW+musYmjZ2iZEYljTAswHb3BuxXz+eK9E8cRWWaNpl/+EPSXv02CxAYA98u7dBn1MAjnBQ0vqtWcbSDQIPvXOc/9ggVKNM5BKnkI23YwYBQwIAsciqOa6jSbVbYkR6qepQJe4jLYmLyNfUH1VI/yejWVfaGLEL3pb9Ivv7/AKZE3R5P59uCLAJ7H35FZ6xL0hh/ohyOLIjW778BRgEfhJ7spf4zNxfsBtNflgBjDMscOl0Rw1PiPMhea+F+lq3nySE+mVxtBIsqRZJ9x6u2OfFpX9kUBQBvSgOK/eLfb7A5ZfDfg6SJJBMqW8rsKkv0NtoH09+M7694W8xEQNEoEsbOGcD92ptgOO/bKw9rXiGna8cAAhFMCmQCND6qrdIDByxf0t/DbGqAAPJq6rsB+JrJOo9TSCJXYHaTs4tq7kcfgp/PLUOm6RCSZdMDQH1ryB27H75BqdNoHXY7wOt3t2s4vvfv8nFdpeXNPp91RltY+/2VM6d1aOeVgm6quyCoqvuAe5/Q4XMCwAT1OTtoEXe8IO5oXWWLS6Lp8d+WsfPB26easwdP0G4yDTIXP8X7M5Y3x/E34ZxxDZsD5j7rDSdv7FVbw5MxllNfxrx2oCM+/wDixx0oOJX3uWVpnKg8bQIlICsK45GMet9NSEehI137gSibDdDvyf0+MD6R4d1KAM0iuruzKCQpVSKAJY+ojaMcyq1zZ0/CUWFphS6HUAIT2AZueL4Y/bF69NjlgeI+qMqIh6gHWl+oHaQCbHt7ZJ1DpUkGnZy8ZItgAz823YGtv6/zxL1rpj6SB9QdQGeUwlQjyKREziwtcqWG8b/heO14TR2hFOme8dPn9I8paM7h3b32+cgpdV0KSR4tO+r1O1/NAsxuyrtXcqmkoMFHHbm65N51yGLQeW0WqkB86N2jIUtI8T72EjBuASVtjZ9AHNVmS9WeJ9NRaafylmXapcMs8e1T7EEG7BHwb7gU7xJI0/ksN7GYA3z9ZA8zigW5Io/HPYjGU6Ly7LUNg0na5m3XWegKWDTyue3UuA3Nssm+2g4TI4L0+LxBHr4mijhK7SC6NvJP1AFdiMpXdZDNRtBa8jB+tdMnaKOONG4ZLLSerarA+lioAPHY19u2VTwTr0hliJTcXKx/V6DuraChXnm6IYGz9s9cjlAF+heD9IF9j78kcEe/via7f07hkNuB4/zsZXYWoazQ54Ez8n2IVD6t4p0whlCySAhliIVSpLSK5Gz0G72PzXwexF89H6mZY4tS7es7UdGDKVbzCgewu3m7/Ne13gfT+hqIHiV7K1qJWHlsI3j5KUeFIF9/n3xdM00+nlSLyxGzqxUHkBnVgWJHqA2+1/HPsyQWy0726QNuZ05CUbGy4Tpv5/wPUwOKsfRtOGhK6kSSu5YHYY9rHezKbsX6ewvtlT0viVElnjm9R3sl7Gp9tptIUhLAC2askXfN4R0p5o2SKldQdwLqa3EimWiAOT2N19rzfUfDgQvIZApkYOFf6f3jEr6/bvXIwWYbPIeqH4xrCDSPWfLxQOu6+UhMSx+Z+7VZnfbvANgtHXOymrb2X8OcK6ZOZWLoAUj6cYSATuZgXUKFUhhe49j297xPqQRIqOAu0ujihuKsGDISBbcjjvVms50XSXSWFl37QT8q2w8myO4vv+P3ykUGveKbdTp1Pzw1UprZGl77tGvGL6fk30lWmOCeKWLehqHSSQBjtWEO+pljZTI7hQ20AgWSbA9xjjrPUfI0DsIvM3bYgCxRVSVWHmGhz/DxYB3DnK9J1/8AcIJGuOGaRSWNXs2gDnlyCxPFnt7Yz61q/N6XKSEpCFiqzakbYFTaKBBdTRo8j4oZUpii8tmYdHCYPz7pVN5qMDiIkTxi3H5zhd9I8SxMuwrIuzagblt1KfMJUKDHt2kkG+ObvjCtNpJdVr2iWRRFAkayErfDEyADkU5/1u1E8GqynaDqKJ0xtqklZvLZwxDASRv6qHufWt9gAPfPQehaN9PoY9tb1jDuQbDPt9QJ9+KH5YdKgaji18Rr1/tMJa1oyG5t0/g/JCtum0kS0wXc4HDv6n/3j2/KhlZ/tE6sVgWHaG3ODyAduz1BlvsbrkdsadL15ZQe94B4y8J6ufYYvJLI17ZHpTakc8c/UDWXvaynTIEC1kFEjtWudxVI6NpG1znTh63IWLntJsZW27bG7kDngXWKOoacQOsaFWJlKEg8FY67k/S3JBHsVq89J8G+C9RpWaaVojI21eHG1IxbGuB3IvgVSj4wjrnhGR9Ss0Hk1uViGZatmqS1A9V235198hNMVWQagbqSOPDx/gJ+IxH+0VAzMRAB0j8X59Y085PhzXnlCNpAIsoOCPi8zPT2QKWXbH6WZTSirViD+oOaxzfpLYHeHl+FA76u6T3D5/leFQacIF3liDRNHbwRyLAv/wBMvfQfGK/syebrWgIFLGiSELGvCchTfA+c3mYrGU25Mo4+Kdh3nNPJHp4sgb/3jqG/AOP/ANRhem6vGw/71q2/xH/iwzWZnhVyW2lei10nQI+LVof4dY//ANyMf1mGGxsDyNNqP8UkX/CY5mZk3an5P3TWyQuv2cntpB/imA/orZFLpXPbSQAEgcys327eUP65mZmkjgm5eZXLdIn3hFj0qn3Gx2I7++8DJdHpJXUsuoARQTccSDt7ASAn3H88zMylzAw24nYbKUVC5t1tdBIIxIdRMQ1bQBEhKnux2gUP1+2C+II30sMkwkmLRxSOGaV2TeB6R5ZNMOQeePaszMwWE+o3K6ZnxXlPUPFc+oVVk1B9BtdqlGuqslaFVfGNYf7QdV5extkiqKDbNrBzzubmifwUd/xGZmZ7vY0z/wAjyXlMrvdvt9131zxaNREI9tCjwS17Q9qQR2PB5sHk/bEcHUXRHRVVvMCbr3CykhK8huOWb8mzWZmim1h7o0SqlZ7hJPht5eJUsepSJmaAbHMcoPHdWPC/A9LlbzXTta6y6YEWqFkRaH/iRsl9+4ocn4HfMzMPEAVc2cTIM+3sSgw7nMdlBtI8dNeOiaaLUqqwltO/mrJC6OTDw0coOw7edhB9ue+XROuxyEh5mVfYKnb8e36ZmZk9Six1R1Qi+k8oB91RhnkUWgc/cpdMYkGpZJAwnG3lWXvGEIfg77A+1bveuVHiPVBAoSNYqMYdUrkl65I70MzMzI74PCVTMUiOJCbSpYV+1UfzBWjgHWZP3qLZ/eFwQeRvET7fbjsPt/XMzMaVOEDHMV1SzimuNXINjfREbixyCWUteO+pa5tQilGRDDwQ/mu0jMF9KsSaoITyQLP88zMSHObjaeUwePmvYpYenV+m1C8aW8+z/wDRHuqV4ymXz10wAVIWmJcKNzM53OSB3IKkA5N4bj3xTRgEmWCRlUuwUSRruDbfpvgkGuCPbNZmVvEszHU/g+5K8BtQtygdPKR7BCHSELNpxIrBwpUhSELROeTYv6ZHN1d/lnoGl1hMA2uQfKVXYFtu+zu47lbJPIzMzM+oUxTpUi3/AKDSfT7leh9NdNaoDsXAdJKa+GyUhisk3HHYIFg7Rfbi7vtxl7brUbEDypbYcGovb8X/ADzMzK3Um1KbSeE+YST3SY+bfwhtb1qMKxaGWhwfTpzV0qnl+a5/3jiKf+0bRJJsMU240/EcP0sxAH1jsQczMzz61NrKwaBsT5SrWMLsOXyZBA8F5n17xrKuq1CrqUVfOmIV4nLKDIx2krYNXXGazMzKKeJqBog7c15BoNn8D7L/2Q==",

    // ... Ajoutez d'autres URLs d'images
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Questions /Réponses")),
      ),
      backgroundColor: Colors.blueGrey,
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 30),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "${context.read<QuestionsQuizCubit>().index + 1}/${context.read<QuestionsQuizCubit>().questions.length}",
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Question",
                        style: TextStyle(
                            fontSize: 17,
                            color: Colors.white,
                            fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                  Spacer(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "${context.read<QuestionsQuizCubit>().score}",
                        style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Points",
                        style: TextStyle(
                            fontSize: 17, color: Colors.white, fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
            ),
            // Utilisation de l'URL d'image correspondante pour chaque question
            Container(
              alignment: Alignment.center,
              child: AspectRatio(
                aspectRatio: 16 / 9, // Remplacez par le rapport hauteur/largeur souhaité
                child: CachedNetworkImage(
                  imageUrl: imageUrls[context.read<QuestionsQuizCubit>().index],
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              context.read<QuestionsQuizCubit>().questions[context.read<QuestionsQuizCubit>().index].question +"?",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w500),
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                      child: GestureDetector(
                        onTap: () {
                          context.read<QuestionsQuizCubit>().checkAnswer(true, context);
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.lightGreen,
                              borderRadius: BorderRadius.circular(24)),
                          child: Text(
                            "True",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: GestureDetector(
                        onTap: () {
                          context.read<QuestionsQuizCubit>().checkAnswer(false, context);
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.orangeAccent,
                              borderRadius: BorderRadius.circular(24)),
                          child: Text(
                            "False",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w400),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            context.read<QuestionsQuizCubit>().nextQuestion(context);
                          });

                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 12),
                          decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(24)),
                          child: const Icon(
                            Icons.arrow_forward_outlined,
                            color: Colors.white,

                          ),
                        ),
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
