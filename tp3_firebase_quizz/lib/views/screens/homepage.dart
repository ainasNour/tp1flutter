import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:tp3_firebase_quizz/views/screens/play_quiz.dart';

import '../../Auth/AuthSelectionScreen.dart';
import '../../Auth/AuthService.dart';
import 'add_question_page.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Color quizButtonColor;
  late Color addQuestionButtonColor;
  late Timer timer;

  final AuthService _authService = AuthService();
  final FirebaseAuth _auth = FirebaseAuth.instance; // Déclarez _auth comme une variable de classe


  User? _currentUser;

  @override
  void initState() {
    super.initState();
    // Initialiser les couleurs et le timer
    quizButtonColor = Colors.blueAccent;
    addQuestionButtonColor = Colors.deepPurpleAccent;
    timer = Timer.periodic(Duration(seconds: 3), (Timer t) {
      // Mettre à jour les couleurs toutes les 5 secondes
      updateButtonColors();
    });
    _getCurrentUser();
  }

  void updateButtonColors() {
    setState(() {
      // Changer les couleurs ici
      quizButtonColor = Colors.greenAccent;
      addQuestionButtonColor = Colors.pinkAccent;
    });
  }

  Future<void> _getCurrentUser() async {
    User? user = _auth.currentUser;

    if (user != null) {
      setState(() {
        _currentUser = user;
      });
    }
  }

String imageURL = '';
  Future<void> pickImage() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);

      DocumentReference imageDoc = await FirebaseFirestore.instance.collection('images').add({
        'url':''
      });

      Reference imageRef = FirebaseStorage.instance.ref(imageDoc.id + '.jpg');
      await imageRef.putFile(imageFile);

      imageURL = await imageRef.getDownloadURL();
      imageDoc.update({
        'url':imageURL
      });

      setState(() { });
      }
    }



  Future<SizedBox> _getCard() async {
    await pickImage(); // Invoke pickImage method to get the imageURL

    return SizedBox(
      width: 500,
      height: 300,
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () async {
                await pickImage(); // Invoke pickImage method when tapping on the CircleAvatar
                setState(() {});
              },
              child: CircleAvatar(
                backgroundColor: Colors.blue,
                radius: 80,
                backgroundImage: imageURL.isNotEmpty ? NetworkImage(imageURL) : null,
              ),
            ),
            ListTile(
              title: Text("Bonjour ${_currentUser?.email ?? 'Utilisateur'}"),
              leading: const Icon(Icons.mail),
            )
          ],
        ),
      ),
    );
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text("Questions /Réponses"),
        ),
      ),
      backgroundColor: Colors.transparent, // Permet de rendre le fond transparent
      body: Stack(
        children: [
        // Arrière-plan avec l'image
        Image.asset(
        "assets/images/q.jpg", // Remplacez par le chemin de votre image de fond
        fit: BoxFit.cover,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
      ),
      Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            FutureBuilder<SizedBox>(
              future: _getCard(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator(); // or a placeholder widget
                } else if (snapshot.hasError) {
                  return Text("Error: ${snapshot.error}");
                } else {
                  return snapshot.data!;
                }
              },
            ),
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => PlayQuiz()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12,horizontal: 54),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(24)
                ),
                child: Text("Lancer le quiz ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: (){
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => AddQuestionPage()
                ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12,horizontal: 54),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(24)
                ),
                child: Text("Ajouter une Question ", style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),),
              ),
            ),
            SizedBox(height: 20),  // Ajout d'un espace vertical
            GestureDetector(
              onTap: () async {
                await _authService.signOut();
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => AuthSelectionScreen()),
                );
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 54),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Text(
                  "Déconnexion",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
        ],
      ),
    );
  }
}