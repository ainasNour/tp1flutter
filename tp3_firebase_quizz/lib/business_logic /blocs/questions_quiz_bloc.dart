
import 'package:bloc/bloc.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../data/models/question.dart';
import '../../data/providers/provider.dart';
import '../../views/screens/result.dart';
import 'QuestionsQuizState.dart';

import 'package:audioplayers/audioplayers.dart';

class QuestionsQuizCubit extends Cubit<QuestionsQuizState> {

  List<Question> _questions=[] ;
  int _index=0 ;
  int _score=0;

  QuestionsQuizCubit(this._questions,this._index,this._score) : super(QuestionsQuizInitial(_questions,0,0));


  void nextQuestion(BuildContext context) {
    if (index < questions.length - 1) {
      index++;
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Result(
                  score: _score
              )));
    }
    emit(QuestionsQuizInitial(questions, index, score));
  }
  AudioPlayer audioPlayer = AudioPlayer();
  void checkAnswer(bool userChoice, BuildContext context) async {
    if (questions[index].isCorrect == userChoice) {
      await playAudioFromFirebase("win.mp3");
      _score++;
      nextQuestion(context);
    } else {
      await playAudioFromFirebase("lose.mp3");
      _score--;
      nextQuestion(context);
    }
  }

  Future<void> playAudioFromFirebase(String audioFileName) async {
    try {
      String audioUrl = await downloadAudioUrl(audioFileName);
      await audioPlayer.play(audioUrl, isLocal: false);
    } catch (e) {
      print("Erreur lors de la lecture du fichier audio : $e");
    }
  }

  Future<String> downloadAudioUrl(String audioPath) async {
    Reference audioReference = FirebaseStorage.instance.ref().child(audioPath);
    String downloadURL = await audioReference.getDownloadURL();
    return downloadURL;
  }
  void restart(){
    index =0;
    score=0;
    _questions = DataProvider.getAllQuestions();
    emit(QuestionsQuizInitial(questions, index, score));
  }

  int get score => _score;

  set score(int value) {
    _score = value;
  }

  int get index => _index;

  set index(int value) {
    _index = value;
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
  }

}
