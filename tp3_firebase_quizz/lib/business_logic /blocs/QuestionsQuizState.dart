
import 'package:flutter/cupertino.dart';

import '../../data/models/question.dart';

@immutable
abstract class QuestionsQuizState {}

class QuestionsQuizInitial extends QuestionsQuizState {
  List<Question> questions ;
  int index;
  int score;
  QuestionsQuizInitial(this.questions, this.index, this.score);

}
