
import 'package:flutter/cupertino.dart';

import '../models/question.dart';
import '../repositories/questionsRepositories.dart';
class DataProvider{

  final QuestionsRepository repository = new QuestionsRepository();

  static List<Question>getAllQuestions(){
    QuestionsRepository.getAllQuestions();
    return QuestionsRepository.questions;
  }
  static void addQuestion(String text, String isCorrect, String thematic,BuildContext context ){
    bool isC = false;
    isCorrect = isCorrect.toLowerCase();
    if(isCorrect== "vrai"){
      isC = true;
    }
    Question question = Question(question: text, isCorrect: isC, thematic: thematic);
    QuestionsRepository.addQuestions(question, context);
  }
}