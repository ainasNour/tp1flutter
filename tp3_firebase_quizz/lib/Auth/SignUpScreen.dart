import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tp3_firebase_quizz/views/screens/homepage.dart';
import 'AuthService.dart';

class SignUpScreen extends StatelessWidget {
  final AuthService _authService = AuthService();




  @override
  Widget build(BuildContext context) {
    String email = '';
    String password = '';

    return Scaffold(
      appBar: AppBar(title: Text('Sign Up')),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            TextFormField(
              onChanged: (val) => email = val,
              decoration: InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              obscureText: true,
              onChanged: (val) => password = val,
              decoration: InputDecoration(labelText: 'Password'),
            ),
            ElevatedButton(
              child: Text('Sign Up'),
              onPressed: () async {
                User? user = await _authService.register(email, password);
            if (user != null) {

    // L'inscription a réussi, vous pouvez naviguer vers une autre page, par exemple.
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                  print('Inscription réussie avec l\'utilisateur: ${user.uid}');
                } else {
                  print('Erreur lors de l\'inscription');
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
