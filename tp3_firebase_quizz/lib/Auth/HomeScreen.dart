// A simple screen displayed after a successful login.
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AuthService.dart';

class HomeScreen extends StatelessWidget {
  final AuthService _authService = AuthService(); // uses the authenticatio service

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          TextButton(
            onPressed: () async {
              await _authService.signOut(); // sign-out
            },
            child: Text('Logout', style: TextStyle(color: Colors.white)),
          ),
        ],
      ),
      body: Center(child: Text('Welcome to the Home Screen!')),
    );
  }
}