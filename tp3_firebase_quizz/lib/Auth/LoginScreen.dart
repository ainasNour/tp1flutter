// screens/login_screen.dart
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'AuthService.dart';
import 'package:tp3_firebase_quizz/views/screens/homepage.dart';

// A screen where users can sign in using their email and password.
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final AuthService _authService = AuthService(); // uses the authentication service
  String email = '';
  String password = '';
  String errorMessage = ''; // Variable to store error message

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        actions: [
          IconButton(
            icon: Icon(Icons.login),
            onPressed: () async {
              // Sign-in and get the result
              User? user = await _authService.signIn(email, password);

              // Check the result and update the state accordingly
              if (user != null) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                );
              } else {
                // Update the state to show the error message
                setState(() {
                  errorMessage = 'Email ou mot de passe incorrect.';
                });
              }
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            TextFormField(
              onChanged: (val) => email = val, // get the email
              decoration: InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              obscureText: true,
              onChanged: (val) => password = val, // get the password
              decoration: InputDecoration(labelText: 'Password'),
            ),
            // Display error message if any
            if (errorMessage.isNotEmpty)
              Text(
                errorMessage,
                style: TextStyle(color: Colors.red),
              ),
          ],
        ),
      ),
    );
  }
}
