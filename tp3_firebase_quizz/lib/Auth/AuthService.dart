// services/auth_service.dart
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart'; // Firebase Authentication dep
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';

// a service to abstract and encapsulate interactions with Firebase Authentication
class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance; // Firebase Authentication singleton
// Sign in with Email & Password
  Future<User?> signIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email,
          password: password);
      return result.user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  // Register with Email & Password
  Future<User?> register(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(email:
      email, password: password);
      return result.user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
// Sign out
  Future<void> signOut() async {
    await _auth.signOut();
  }

}
    