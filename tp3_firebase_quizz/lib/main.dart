import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp3_firebase_quizz/views/screens/homepage.dart';

import 'Auth/AuthSelectionScreen.dart';
import 'Auth/LoginScreen.dart';
import 'business_logic /blocs/QuestionsQuizState.dart';
import 'business_logic /blocs/questions_quiz_bloc.dart';
import 'data/providers/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => QuestionsQuizCubit(DataProvider.getAllQuestions(), 0, 0),
      child: BlocBuilder<QuestionsQuizCubit, QuestionsQuizState>(
        builder: (_, theme) {
          return MaterialApp(
            debugShowCheckedModeBanner: true,
            theme: ThemeData(
              primarySwatch: Colors.blueGrey,
            ),
            home: StreamBuilder<User?>(
              // listen to changes in Firebase Authentication to manage user sessions
              stream: FirebaseAuth.instance.authStateChanges(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return HomePage(); // user is already signed-in
                }
                return AuthSelectionScreen(); // user should sign in
              },
            ),
          );
        },
      ),
    );
  }
}
